package com.moscii.starcatagent.fragment;


import android.app.ActivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moscii.starcatagent.adapter.DeviceAdapter;
import com.moscii.starcatagent.model.Content;
import com.moscii.starcatagent.R;
import com.moscii.starcatagent.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeviceFragment extends Fragment {


    public DeviceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_device, container, false);

        RecyclerView rv = view.findViewById(R.id.recycler_view_hardware);
        rv.setHasFixedSize(true);

        List<Content> inventories = new ArrayList<>();

        inventories.add(new Content("Brand", Utility.getBrand()));
        inventories.add(new Content("Model", Utility.getModel()));

        inventories.add(new Content("OS version", Utility.getOsVersion()));
        inventories.add(new Content("Android Version", Utility.getAndroidVersion()));
        inventories.add(new Content("SDK Version", Utility.getSdkVersion()));

        inventories.add(new Content("IPAddress", Utility.getIpAddress()));
        inventories.add(new Content("MACAddress", Utility.getMACAddress()));
        inventories.add(new Content("Location", Utility.getLocation(getContext())));

        ActivityManager.MemoryInfo info = Utility.getMemoryInfo(getContext());

        inventories.add(new Content("Memory Total", String.valueOf(info.totalMem / 0x100000L)));
        inventories.add(new Content("Memory Available", String.valueOf(info.availMem / 0x100000L)));
        inventories.add(new Content("Memory Percent", String.valueOf(info.availMem / (double)info.totalMem * 100.0)));

        inventories.add(new Content("Battery Info", String.valueOf(Utility.getBatteryInfo(getContext()) + " %")));

        DeviceAdapter adapter = new DeviceAdapter(inventories);
        rv.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        return view;
    }

}

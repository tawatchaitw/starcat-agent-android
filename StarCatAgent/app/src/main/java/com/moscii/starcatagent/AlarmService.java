package com.moscii.starcatagent;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by diary on 16/11/2017 AD.
 */

public class AlarmService extends Service {
    Alarm alarm = new Alarm();
    public void onCreate()
    {
        super.onCreate();
        Log.d("AlarmService", "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.d("AlarmService", "onStartCommand");
        alarm.setAlarm(this);
        return START_STICKY;
    }

    @Override
    public void onStart(Intent intent, int startId)
    {
        Log.d("AlarmService", "onStart");
        alarm.setAlarm(this);
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        Log.d("AlarmService", "onBind");
        return null;
    }
}

package com.moscii.starcatagent.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Device {
    private String Id;
    private String Name;
    private String IpAddress;
    private String MacAddress;
    private Double Latitude;
    private Double Longitude;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getIpAddress() {
        return IpAddress;
    }

    public void setIpAddress(String ipAddress) {
        IpAddress = ipAddress;
    }

    public String getMacAddress() {
        return MacAddress;
    }

    public void setMacAddress(String macAddress) {
        MacAddress = macAddress;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public String toJson() {
    JSONObject jsonObject = new JSONObject();
    try {
        jsonObject.put("Id", getId());
        jsonObject.put("Name", getName());
        jsonObject.put("IPAddress", getIpAddress());
        jsonObject.put("MACAddress", getMacAddress());
        jsonObject.put("Latitude", getLatitude());
        jsonObject.put("Longitude", getLongitude());
        jsonObject.put("Status", true);


        return jsonObject.toString();
     }
     catch (JSONException e) {
        e.printStackTrace();

        return "";
    }

    }
}

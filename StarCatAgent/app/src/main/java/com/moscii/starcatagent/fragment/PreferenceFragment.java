package com.moscii.starcatagent.fragment;


import android.annotation.TargetApi;
import android.app.AlertDialog;
//import android.app.FragmentManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.moscii.starcatagent.DeviceAdminReceiver;
import com.moscii.starcatagent.R;
import com.moscii.starcatagent.activity.MainActivity;
import com.moscii.starcatagent.activity.ResetPasswordWithTokenActivity;
import com.moscii.starcatagent.adapter.PreferenceAdapter;
import com.moscii.starcatagent.model.Content;
import com.moscii.starcatagent.policy.resetpassword.ResetPasswordWithTokenFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreferenceFragment extends Fragment {

    private ComponentName mAdminComponentName;
    private DevicePolicyManager mDevicePolicyManager;

    public PreferenceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_preference, container, false);

        mDevicePolicyManager = (DevicePolicyManager) getActivity().getSystemService(Context.DEVICE_POLICY_SERVICE);
        mAdminComponentName = DeviceAdminReceiver.getComponentName(getActivity());

        RecyclerView rv = view.findViewById(R.id.recycler_view_preference);
        rv.setHasFixedSize(true);

        List<Content> contents = new ArrayList<>();

        contents.add(new Content("App Status", getAppStatus()));

        PreferenceAdapter adapter = new PreferenceAdapter(contents);
        rv.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        return view;

//        //Integer type = loadAppStatus();
//
//        Switch disableCamera = view.findViewById(R.id.switch_disable_camera);
//        disableCamera.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
//           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//               mDevicePolicyManager.setCameraDisabled(mAdminComponentName, isChecked);
//           }
//        });
//
//        Switch disableScreenCapture = view.findViewById(R.id.switch_disable_screen_capture);
//        disableScreenCapture.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
//            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                mDevicePolicyManager.setScreenCaptureDisabled(mAdminComponentName, isChecked);
//            }
//        });
//
//        TextView wipeData = view.findViewById(R.id.textView_wipe_data);
//        wipeData.setOnClickListener((View v) -> {
//            showWipeDataPrompt();
//        });
//
//        TextView resetPasswordToken = view.findViewById(R.id.textView_reset_password_token);
//        resetPasswordToken.setOnClickListener((View v) -> {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                Intent intent = new Intent(getActivity(), ResetPasswordWithTokenActivity.class);
//                startActivity(intent);
//            } else {
//                showResetPasswordPrompt();
//            }
//        });
//
//        return view;
    }

    private void showWipeDataPrompt() {
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.wipe_data_dialog_prompt, null);
        final CheckBox externalStorageCheckBox = (CheckBox) dialogView.findViewById(
                R.id.external_storage_checkbox);
        final CheckBox resetProtectionCheckBox = (CheckBox) dialogView.findViewById(
                R.id.reset_protection_checkbox);

        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.wipe_data_title)
                .setView(dialogView)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                int flags = 0;
                                flags |= (externalStorageCheckBox.isChecked() ?
                                        DevicePolicyManager.WIPE_EXTERNAL_STORAGE : 0);
                                flags |= (resetProtectionCheckBox.isChecked() ?
                                        DevicePolicyManager.WIPE_RESET_PROTECTION_DATA : 0);
                                mDevicePolicyManager.wipeData(flags);
                            }
                        })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private Integer loadAppStatus() {
        final @StringRes int appStatusStringId;
        if (mDevicePolicyManager.isDeviceOwnerApp(getActivity().getPackageName())) {
            appStatusStringId = R.string.this_is_a_device_owner;
        } else {
            appStatusStringId = R.string.this_is_not_an_admin;
        }
        return appStatusStringId;
    }

    private String getAppStatus() {
        String status;
        if (mDevicePolicyManager.isDeviceOwnerApp(getActivity().getPackageName())) {
            status = "This app is the device owner";
        } else {
            status = "This app is not an admin";
        }
        return status;
    }


    private void showFragment(final Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().addToBackStack(PreferenceFragment.class.getName())
                .replace(R.id.container, fragment).commit();
    }

    private void showFragment(final Fragment fragment, String tag) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().addToBackStack(PreferenceFragment.class.getName())
                .replace(R.id.container, fragment, tag).commit();
    }

    /**
     * Shows a prompt to ask for a password to reset to and to set whether this requires
     * re-entry before any further changes and/or whether the password needs to be entered during
     * boot to start the user.
     */
    private void showResetPasswordPrompt() {
        View dialogView = getActivity().getLayoutInflater().inflate(
                R.layout.reset_password_dialog, null);

        final EditText passwordView = (EditText) dialogView.findViewById(
                R.id.password);
        final CheckBox requireEntry = (CheckBox) dialogView.findViewById(
                R.id.require_password_entry_checkbox);
        final CheckBox dontRequireOnBoot = (CheckBox) dialogView.findViewById(
                R.id.dont_require_password_on_boot_checkbox);

        DialogInterface.OnClickListener resetListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                String password = passwordView.getText().toString();
                if (TextUtils.isEmpty(password)) {
                    password = null;
                }

                int flags = 0;
                flags |= requireEntry.isChecked() ?
                        DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY : 0;
                flags |= dontRequireOnBoot.isChecked() ?
                        DevicePolicyManager.RESET_PASSWORD_DO_NOT_ASK_CREDENTIALS_ON_BOOT : 0;

                boolean ok = false;
                try {
                    ok = mDevicePolicyManager.resetPassword(password, flags);
                } catch (IllegalArgumentException | IllegalStateException | SecurityException e) {
                    // Not allowed to set password or trying to set a bad password, eg. 2 characters
                    // where system minimum length is 4.
                    //Log.w(TAG, "Failed to reset password", e);
                }
                //showToast(ok ? R.string.password_reset_success : R.string.password_reset_failed);
            }
        };

        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.reset_password)
                .setView(dialogView)
                .setPositiveButton(android.R.string.ok, resetListener)
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

}

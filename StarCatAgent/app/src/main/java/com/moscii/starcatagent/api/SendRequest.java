package com.moscii.starcatagent.api;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.moscii.starcatagent.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class SendRequest extends AsyncTask <String, Integer, Boolean> {

    private Context mContext;

    public SendRequest (Context context){
        mContext = context;
    }

    protected Boolean doInBackground(String... params) {

        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod(params[1]);
            httpURLConnection.setRequestProperty("Content-Type", "application/json");

            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
            bufferedWriter.write(params[2]);
            bufferedWriter.flush();

            BufferedReader in=new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            StringBuffer sb = new StringBuffer("");
            String line;

            while((line = in.readLine()) != null) {
                sb.append(line);
                break;
            }

            in.close();
            String res = sb.toString();

            int code = httpURLConnection.getResponseCode();
            if (code == HttpURLConnection.HTTP_OK) {
                JSONObject jsonObject = new JSONObject(res);
                String agentId = jsonObject.getString("id");

                SharedPreferences sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.preference_agent_info_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(mContext.getString(R.string.agent_id_key), agentId);
                editor.apply();
                return true;
            }

        } catch (Exception e) {
            Log.d("ApiRequest", "Exception : " + String.valueOf(e));
            e.printStackTrace();
        }

        return false;
    }

    protected void onProgressUpdate(Integer... progress) {

    }

    protected void onPostExecute(Boolean result) {

    }


}

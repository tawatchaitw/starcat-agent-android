package com.moscii.starcatagent.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moscii.starcatagent.R;
import com.moscii.starcatagent.model.Content;

import java.util.List;

public class PreferenceAdapter extends RecyclerView.Adapter<PreferenceAdapter.ViewHolder> {
    private List<Content> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public TextView mNameView;
        public TextView mValueView;
        public ViewHolder(View v) {
            super(v);

            mCardView = v.findViewById(R.id.card_view_preference);
            mNameView = v.findViewById(R.id.name_preference);
            mValueView = v.findViewById(R.id.value_preference);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PreferenceAdapter(List<Content> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PreferenceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item_preference, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mNameView.setText(mDataset.get(position).getName());
        holder.mValueView.setText(mDataset.get(position).getValue());
        //holder.mTextView.setText("Add 8dp of padding at the top and bottom of a list, except for lists with subheaders, which have their own padding.");
        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String currentValue = mDataset.get(position).getName();
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}

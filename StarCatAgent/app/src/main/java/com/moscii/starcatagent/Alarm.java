package com.moscii.starcatagent;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import com.moscii.starcatagent.activity.MainActivity;
import com.moscii.starcatagent.api.SendRequest;
import com.moscii.starcatagent.model.Device;

import java.util.UUID;

/**
 * Created by diary on 16/11/2017 AD.
 */

public class Alarm extends BroadcastReceiver {

    public void updateDevice(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_agent_info_key), Context.MODE_PRIVATE);
        String agentId = sharedPref.getString(context.getString(R.string.agent_id_key), null);

        if(agentId != null) {
            GpsTracker gps = new GpsTracker(context);

            Device device = new Device();
            device.setId(agentId);
            device.setName("Android - " + Utility.getBrand());
            device.setIpAddress(Utility.getIpAddress());
            device.setMacAddress(Utility.getMACAddress());
            device.setLatitude(gps.getLatitude());
            device.setLongitude(gps.getLongitude());
            new SendRequest(context).execute("http://pod.startraxgps.com/api/AndroidDevice/" + agentId, "PUT", device.toJson());
        }
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        updateDevice(context);
        Toast.makeText(context, "Alarm !!!!!!!!!!", Toast.LENGTH_LONG).show(); // For example

        wl.release();
    }

    public void setAlarm(Context context)
    {
        cancelAlarm(context);
//        boolean any = checkAlarm(context);
//        if (!any)
//        {
//            AlarmManager alarmManager =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
//            Intent intent = new Intent(context, Alarm.class);
//            PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
//            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 180, sender);
//        }
    }

    public boolean checkAlarm(Context context){
        Intent intent = new Intent(context, Alarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE);
        return sender != null;
    }

    public void cancelAlarm(Context context)
    {
        Intent intent = new Intent(context, Alarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}

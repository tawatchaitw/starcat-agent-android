package com.moscii.starcatagent.fragment;


import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moscii.starcatagent.adapter.PackageAdapter;
import com.moscii.starcatagent.model.Content;
import com.moscii.starcatagent.R;
import com.moscii.starcatagent.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PackageFragment extends Fragment {


    public PackageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_package, container, false);

        RecyclerView rv = view.findViewById(R.id.recycler_view_software);
        rv.setHasFixedSize(true);

        List<Content> softwareList = new ArrayList<>();
        List<ApplicationInfo> appInfoList = Utility.getPackageInfo(getContext());

        for(ApplicationInfo app : appInfoList) {
            //checks for flags; if flagged, check if updated system app
            if((app.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
                //softwareList.add(new Software(app.packageName, "System app"));

                //it's a system app, not interested
            } else if ((app.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                //softwareList.add(new Software(app.packageName, "Install app"));
                //Discard this one
                //in this case, it should be a user-installed app
            } else {
                PackageManager pm = getActivity().getPackageManager();
//                ApplicationInfo ai;
//                try {
//                    ai = pm.getApplicationInfo( app.packageName, 0);
//                } catch (final PackageManager.NameNotFoundException e) {
//                    ai = null;
//                }
//                final String applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");

                softwareList.add(new Content((String)pm.getApplicationLabel(app), app.packageName));
            }
        }

        if (softwareList.size() > 0) {
            Collections.sort(softwareList, new Comparator<Content>() {
                @Override
                public int compare(final Content object1, final Content object2) {
                    return object1.getName().compareTo(object2.getName());
                }
            });
        }


        PackageAdapter adapter = new PackageAdapter(softwareList);
        rv.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);


        return view;
    }



}
